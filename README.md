# LEEME #

Guia de isntalación de las pruebas para Zinobe

### ¿Cómo instalar? ###

* Se debe modificar el archivo variables.php donde deben modificar los valores correspondientes a la base de datos y a la url del proyecto.
* si el nombre de la base de datos se cambia en el archivo variables.php es necesario modificar el archivo bd.sql para cambiar la base de datos que debe crear.
* Luego de eso en el terminal en la carpeta del proyecto se debe ejecutar el comando composer install para que se genere el vendor.
* Luego de eso se debe ingresar al sitio y automáticamente ejecutará el sql inicializando y creando la base de datos.