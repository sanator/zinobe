<?php
include_once('../classes/ConexionClass.php');
/**
 * Se genera un trait de conexión para las funciones del mismo
 */
trait GeneralTrait
{
    public function traerPaises(){
        $conexion = new Conexion;
        $conexion_abierta = $conexion->abrirConexion();
        $sql = "SELECT id, pais, iso_code FROM paises ORDER BY pais ASC";
        $resultado = $conexion_abierta->query($sql);
        $paises = array();
        if ($resultado->num_rows > 0) {
            while($pais = $resultado->fetch_assoc()) {
                $paises[] = $pais;
            }
        }

        $conexion->cerrarConexion();
        return $paises;
    }

    public function insertar($tabla, $campos, $valores){
        $conexion = new Conexion;
        $conexion_abierta = $conexion->abrirConexion();
        $sql = "INSERT INTO ".$tabla." (".$campos.") VALUES (";
        foreach($valores as $valor){
            if(is_int($valor)){
                $sql .= $valor.",";
            }else{
                $sql .= "'".$valor."',";
            }
        }
        $sql = substr($sql, 0, -1);
        $sql .= ")";
        $resultado = $conexion_abierta->query($sql);
        $conexion->cerrarConexion();
        if($resultado){
            $mensaje = 'Registro creado correctamente.';
        }else{
            $mensaje = 'El registro no pudo ser creado';
        }
        return array($resultado,$mensaje);
    }

    public function redirect($url = ''){
        header('Location: '.variables\PAGINA.'/'.$url);
        exit();
    }
}
