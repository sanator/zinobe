<?php
include_once('baseClass.php');

class Users extends base{
    use GeneralTrait;

    public function __construct(){
        base::inicializar();
    }

    public function getRegistrarUsuario($mensaje = ''){
        if(!$this->usuario){
            $paises = GeneralTrait::traerPaises();
            $template = $this->twig->load('users/create.html');
            return $template->render(array('paises' => $paises, 'mensaje' => $mensaje));
        }else{
            return GeneralTrait::redirect('usuarios');
        }
    }

    public function postRegistrarUsuario(){
        if(!$this->usuario){
            $input = array();
            if(trim($_REQUEST['pais']) != ''){            
                $input['pais'] = $_REQUEST['pais'];
            }else{
                return $this->getRegistrarUsuario('Por favor selecciona un pais.');                
            }
            if(strlen(trim($_REQUEST['nombre'])) >= 3 && trim($_REQUEST['nombre']) != ''){
                $input['nombre'] = $_REQUEST['nombre'];
            }else{
                return $this->getRegistrarUsuario('El nombre debe tener un mínimo de 3 caracteres.');
            }
            if(trim($_REQUEST['email']) != ''){
                $input['email'] = $_REQUEST['email'];
            }else{
                return $this->getRegistrarUsuario('Por favor ingresa un email válido.');                
            }
            if(trim($_REQUEST['password']) != '' && strlen($_REQUEST['password']) >= 6 && preg_match('@[0-9]@', $_REQUEST['password']) ){
                $input['password'] = $_REQUEST['password'];
            }else{
                return $this->getRegistrarUsuario('Por favor ingresa una contraseña válida de al menos 6 caracteres y al menos un 1 dígito.');                 
            }
            $insercion = GeneralTrait::insertar('usuarios', 'pais_id, nombre, email, pass', $input);
            $template = $this->twig->load('login.html');
            return $template->render(array('mensaje' => $insercion[1], 'insercion' => $insercion[0]));
        }else{
            return GeneralTrait::redirect('usuarios');
        }
    }

    public function getListarUsuarios(){
        if($this->usuario){
            $template = $this->twig->load('users/list.html');
            return $template->render(array('the' => 'variables', 'go' => 'here'));
        }else{
            return GeneralTrait::redirect();
        }
    }

    public function postListarUsuarios(){
        if($this->usuario){
            $conexion = new Conexion;
            $conexion_abierta = $conexion->abrirConexion();
            $buscador = $_REQUEST['buscador'];
            $sql = "SELECT u.nombre, u.email, p.pais FROM usuarios u INNER JOIN paises p ON p.id = u.pais_id WHERE u.nombre LIKE '%".$buscador."%' OR u.email LIKE '%".$buscador."%'";
            $resultado = $conexion_abierta->query($sql);
            $usuarios = array();
            if ($resultado->num_rows > 0) {
                while($usuario = $resultado->fetch_assoc()) {
                    $usuarios[] = $usuario;
                }
            }
            $conexion->cerrarConexion();
            $template = $this->twig->load('users/list.html');
            return $template->render(array('usuarios' => $usuarios));
        }else{
            return GeneralTrait::redirect();
        }
    }

}
if(isset($_REQUEST['users'])){
    $usuario = new Users;
    $metodo = $_REQUEST['users'];
    echo $usuario->$metodo();
}