<?php
include_once('../variables.php');

class Conexion
{
    //variables
    //use variables\SERVIDOR;
    public $conn = '';
    /**
     * Crear conexión de mysql
     */
    public function abrirConexion(){
        $this->conn = new mysqli(variables\SERVIDOR, variables\USUARIO, variables\PASSWORD, variables\DATABASE);

        if ($this->conn->connect_errno) {
            if ($this->conn->connect_errno == 1049) {
                $this->conn = new mysqli(variables\SERVIDOR, variables\USUARIO, variables\PASSWORD);
                if ($this->conn->connect_errno) {
                    die("Fallo al conectar a MySQL: (" . $this->conn->connect_errno . ") " . $this->conn->connect_error);
                }
                return $this->inicializarBD();
            }else{
                die("Fallo al conectar a MySQL: (" . $this->conn->connect_errno . ") " . $this->conn->connect_error);
            }
        }

        return $this->conn;
    }

    private function inicializarBD(){
        $commands = file_get_contents('../bd.sql');
        $this->conn->multi_query($commands);
        $this->cerrarConexion();
        return $this->abrirConexion();
    }

    public function cerrarConexion(){
        $this->conn->close();
    }
}