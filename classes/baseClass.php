<?php
require_once '../vendor/autoload.php';
include_once('../Traits/GeneralTrait.php');

class base
{
    public $twig;
    public $loader;
    public $usuario;

    public function inicializar(){
        $this->loader = new Twig_Loader_Filesystem('../views/');
        $this->twig = new Twig_Environment($this->loader, array(
            'cache' => '../views_cache/',
            'auto_reload' => true
        ));
        $this->usuario = $this->verificarSesion();
    }

    public function verificarSesion(){
        session_start();
        if(isset($_SESSION['usuario'])){
            return $_SESSION['usuario'];
        }else{
            return false;
        }
    }
}