<?php
include_once('baseClass.php');

class Autentication extends base
{
    use GeneralTrait;

    public function __construct(){
        base::inicializar();
    }

    public function getLogin(){
        if(!$this->usuario){
            $template = $this->twig->load('login.html');
            return $template->render();
        }else{
            return GeneralTrait::redirect('usuarios');
        }
    }

    public function postLogin(){
        if(!$this->usuario){
            $email = $_REQUEST['email'];
            $password = $_REQUEST['password'];
            $conexion = new Conexion;
            $conexion_abierta = $conexion->abrirConexion();
            $sql = "SELECT * FROM usuarios WHERE email = '".$email."' AND pass = '".$password."'";
            $resultado = $conexion_abierta->query($sql);
            if ($resultado->num_rows > 0) {
                $template = $this->twig->load('users/list.html');
                $_SESSION['usuario'] = $resultado->fetch_assoc();
                $conexion->cerrarConexion();
                return GeneralTrait::redirect('usuarios');
            }else{
                $mensaje = 'Hubo un error en la autenticación, es posible que el email o la contraseña estén erradas. Por favor intente nuevamente';
            }
            $conexion->cerrarConexion();
            $template = $this->twig->load('login.html');
            return $template->render(array('mensaje'=> $mensaje));
        }else{
            return GeneralTrait::redirect('usuarios');
        }
    }

    public function postCerrarSesion(){
        session_destroy();
        return GeneralTrait::redirect();
    }
}

if(isset($_REQUEST['autenticacion'])){
    $autenticacion = New Autentication;
    $metodo = $_REQUEST['autenticacion'];
    echo $autenticacion->$metodo();
}